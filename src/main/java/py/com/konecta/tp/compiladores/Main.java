/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.konecta.tp.compiladores;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author Juan Vargas <juan.vargas@konecta.com.py at Konecta S.A>
 */
public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);
    private static String INPUT = "";
    private static String CADENA_DE_ENTRADA = "* + 1 2 2";//1*2+3*4-5
    private static Integer CONTADOR = 0;

    public static void main(String[] args) throws Exception {
        CADENA_DE_ENTRADA = args[0];
        CADENA_DE_ENTRADA = CADENA_DE_ENTRADA.replaceAll(" ", "");
        LOGGER.info("CADENA_DE_ENTRADA: [{}]", CADENA_DE_ENTRADA);
        match(INPUT);
        elemento();
    }

    private static String signoA() throws Exception {
        if (INPUT.equals("+")) {
            match("+");
            return "+";
        } else if (INPUT.equals("-")) {
            match("-");
            return "-";
        } else {
            throw new Exception("Error de cadena");
        }
    }

    private static String signoB() throws Exception {
        if (INPUT.equals("*")) {
            match("*");
            return "*";
        } else if (INPUT.equals("/")) {
            match("/");
            return "/";
        } else {
            throw new Exception("Error de cadena");
        }
    }

    private static void elemento() throws Exception {
        if (INPUT.equals("+") || INPUT.equals("-")) {
            String signoA = signoA();
            String num = numero();
            String num2 = numero();
            LOGGER.info("cadena de salida: {}", num + signoA + num2);
        } else if (INPUT.equals("*") || INPUT.equals("/")) {
            String signoB = signoB();
            String num = numero();
            String num2 = numero();
            LOGGER.info("cadena de salida: {}", num + signoB + num2);
        } else {
            throw new Exception("Error de cadena");
        }
    }

    private static String numero() throws Exception {
        if (INPUT.equals("1")) {
            match("1");
            return "1";
        } else if (INPUT.equals("2")) {
            match("2");
            return "2";
        } else if (INPUT.equals("3")) {
            match("3");
            return "3";
        } else if (INPUT.equals("4")) {
            match("4");
            return "4";
        } else if (INPUT.equals("5")) {
            match("5");
            return "5";
        } else if (INPUT.equals("6")) {
            match("6");
            return "6";
        } else if (INPUT.equals("7")) {
            match("7");
            return "7";
        } else if (INPUT.equals("8")) {
            match("8");
            return "8";
        } else if (INPUT.equals("9")) {
            match("9");
            return "9";
        } else if (INPUT.equals("0")) {
            match("0");
            return "0";
        } else if (INPUT.equals("+") || INPUT.equals("-")) {
            String signoA = signoA();
            String num = numero();
            String num2 = numero();
            return "(" + num + signoA + num2 + ")";
        } else if (INPUT.equals("*") || INPUT.equals("/")) {
            String signoB = signoB();
            String num = numero();
            String num2 = numero();
            return num + signoB + num2;
        } else {
            throw new Exception("Error de cadena");
        }
    }

    private static void match(String character) throws Exception {
        if (character.equals(INPUT)) {
            if (CONTADOR < CADENA_DE_ENTRADA.length()) {
                Character charAt = CADENA_DE_ENTRADA.charAt(CONTADOR++);
                INPUT = charAt.toString();
            } else {
                INPUT = "";
            }
        } else {
            throw new Exception("ERROR!!");
        }
    }
}
